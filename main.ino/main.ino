// what to do with incoming data
volatile byte command = 0;
byte buttonsState = 0b00000001;
byte buttonsOff = 0b00000000;
byte result;

void setup (void)
  {
  Serial.begin(115200);
  // have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);

  // turn on SPI in slave mode
  SPCR |= _BV(SPE);

  // turn on interrupts
  SPCR |= _BV(SPIE);

  }  // end of setup


// SPI interrupt routine
ISR (SPI_STC_vect)
  {
    byte c = SPDR;
    SPDR = result;  // subtract 8


  }  // end of interrupt service routine (ISR) SPI_STC_vect

void loop (void)
  {
      buttonsState = buttonsState << 1;
      if(buttonsState == 0)
      {
        buttonsState = 0b00000001;
      }
      result = buttonsState;
      Serial.println(result);
      delay(1000);
      byte result = buttonsOff;
      delay(1000);
      
  }  
